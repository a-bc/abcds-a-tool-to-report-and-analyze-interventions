# Acyclic Behavior Change Diagrams: A Tool to Report and Analyze Interventions

This repository accompanies "Acyclic Behavior Change Diagrams: A Tool to Report and Analyze Interventions" by Metz, Peters & Crutzen (2022). The corresponding rendered R Markdown file is hosted by GitLab Pages at https://a-bc.gitlab.io/abcds-a-tool-to-report-and-analyze-interventions, and the Open Science Framework repository is located at https://osf.io/epbm4.
